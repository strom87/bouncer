package core

import (
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database/entities"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"bitbucket.org/strom87/bouncer/models"
	"github.com/strom87/encryption"
)

// Authentication struct
type Authentication struct{}

// NewAuthentication returns an instance of Authentication
func NewAuthentication() *Authentication {
	return &Authentication{}
}

// ValidateCredentials validates the user credentials and returns the user and a ResponseCode if ok
func (a Authentication) ValidateCredentials(repository *repositories.UserRepository, model models.Login, secret string) (*entities.User, constants.ResponseCode) {
	enity, _ := repository.GetByEmail(model.Email)
	if enity == nil {
		return nil, constants.NotFound
	}

	p := encryption.NewPasswordHash()
	if ok, _ := p.Match(model.Password, enity.Password, enity.Salt); !ok {
		return nil, constants.PasswordMissMatch
	}

	if model.Platform == constants.Web {
		enity.Web.Secret = secret
	} else if model.Platform == constants.App {
		enity.App.Secret = secret
	} else {
		return nil, constants.InvalidPlatform
	}

	NewAccount().RehashPassword(repository, enity, model.Password)

	return enity, constants.Ok
}
