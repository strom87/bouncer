package core

import (
	"bitbucket.org/strom87/bouncer/database/entities"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"github.com/strom87/encryption"
)

// Account struct
type Account struct{}

// NewAccount returns an instance of Account
func NewAccount() *Account {
	return &Account{}
}

// RehashPassword rehashes the password if needed
func (Account) RehashPassword(repository *repositories.UserRepository, user *entities.User, password string) {
	p := encryption.NewPasswordHash().SetRehashDays(4)
	if ok, _ := p.RehashNeeded(user.Password); ok {
		pass, salt, _ := p.Make(password)
		user.Password = pass
		user.Salt = salt
	}
	repository.Update(user)
}

// UpdatePassword updates the users password
func (Account) UpdatePassword(repository *repositories.UserRepository, userID, password string) error {
	user, err := repository.GetById(userID)
	if user == nil {
		return err
	}

	pass, salt, _ := encryption.NewPasswordHash().Make(password)
	user.Password = pass
	user.Salt = salt
	repository.Update(user)
	return nil
}
