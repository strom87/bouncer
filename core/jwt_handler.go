package core

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database/entities"
	"bitbucket.org/strom87/bouncer/database/repositories"
	jwt "github.com/dgrijalva/jwt-go"
)

// JwtHandler struct
type JwtHandler struct {
	config config.Config
}

// NewJwtHandler returns a instance of JwtHandler
func NewJwtHandler(config config.Config) *JwtHandler {
	return &JwtHandler{config}
}

// CreateToken generates a new token
func (j JwtHandler) CreateToken(user *entities.User, platform string) (string, error) {
	var secret string
	if platform == constants.Web {
		secret = user.Web.Secret
	} else {
		secret = user.App.Secret
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"id":       user.ID.Hex(),
		"iat":      time.Now().Unix(),
		"exp":      time.Now().Add(time.Minute * j.config.Jwt.ExpirationMinutes).Unix(),
		"secret":   secret,
		"platform": platform,
		"iis":      "nearestinjury.com",
	})

	key, err := j.getPrivateKey()
	if err != nil {
		return "", err
	}

	return token.SignedString(key)
}

// ValidateToken checks if the jwt token is valid
func (j JwtHandler) ValidateToken(repository *repositories.UserRepository, tokenString string) (*entities.User, constants.ResponseCode, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		key, err := j.getPublicKey()
		if err != nil {
			return nil, fmt.Errorf("Invalid key %s", err)
		}

		return key, nil
	})
	if err != nil {
		return nil, constants.TokenInvalid, err
	}

	id, platform, secret, claims, err := j.getTokenClaims(token)
	if err != nil {
		return nil, constants.TokenClaimsError, err
	}
	if !claims.VerifyExpiresAt(time.Now().Unix(), true) {
		return nil, constants.TokenExpired, errors.New("Token expired")
	}

	user, err := repository.GetById(id)
	if err != nil {
		return nil, constants.DbConnectionError, err
	} else if user == nil {
		return nil, constants.UserNotFound, errors.New("User not found")
	}

	if platform == constants.Web {
		if user.Web.Secret != secret {
			return nil, constants.TokenSecretMissMatch, errors.New("Token web secret missmatch")
		}
	} else {
		if user.App.Secret != secret {
			return nil, constants.TokenSecretMissMatch, errors.New("Token app secret missmatch")
		}
	}

	return user, constants.Ok, nil
}

func (j JwtHandler) getTokenClaims(token *jwt.Token) (string, string, string, *jwt.MapClaims, error) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return "", "", "", nil, errors.New("Invalid token claims")
	}

	id, ok := claims["id"].(string)
	if !ok {
		return "", "", "", nil, errors.New("Claim id error")
	}
	platform, ok := claims["platform"].(string)
	if !ok {
		return "", "", "", nil, errors.New("Claim platform error")
	}

	secret, ok := claims["secret"].(string)
	if !ok {
		return "", "", "", nil, errors.New("Claim secret error")
	}

	return id, platform, secret, &claims, nil
}

func (j JwtHandler) getPublicKey() (*rsa.PublicKey, error) {
	keyBytes, err := ioutil.ReadFile(j.config.Rsa.PublicKey)
	if err != nil {
		return nil, err
	}

	return jwt.ParseRSAPublicKeyFromPEM(keyBytes)
}

func (j JwtHandler) getPrivateKey() (*rsa.PrivateKey, error) {
	keyBytes, err := ioutil.ReadFile(j.config.Rsa.PrivateKey)
	if err != nil {
		return nil, err
	}

	return jwt.ParseRSAPrivateKeyFromPEM(keyBytes)
}
