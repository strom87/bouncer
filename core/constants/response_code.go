package constants

// ResponseCode type
type ResponseCode int

// ResponseCode types
const (
	Ok ResponseCode = iota
	NotFound
	UnexpectedError
	PasswordMissMatch
	JSONEncodeError
	JSONDecodeError
	TokenCreateError
	TokenInvalid
	TokenExpired
	TokenClaimsError
	TokenSecretMissMatch
	UserNotFound
	UserExists
	DbConnectionError
	InvalidPlatform
	InvalidUserInput
)

var responseCodes = [...]string{
	"Ok",
	"NotFound",
	"UnexpectedError",
	"PasswordMissMatch",
	"JsonEncodeError",
	"JsonDecodeError",
	"TokenCreateError",
	"TokenInvalid",
	"TokenExpired",
	"TokenClaimsError",
	"TokenSecretMissMatch",
	"UserNotFound",
	"UserExists",
	"DbConnectionError",
	"InvalidPlatform",
	"InvalidUserInput",
}

func (r ResponseCode) String() string {
	return responseCodes[r]
}

// ToString returns a string value of the ResponseCode
func (r ResponseCode) ToString() string {
	return responseCodes[r]
}
