package routes

import (
	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/controllers"
	"bitbucket.org/strom87/bouncer/middleware"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
)

// Init the routes
func Init(config config.Config) *mux.Router {
	authController := controllers.NewAuthenticationController(config)
	tokenController := controllers.NewTokenController(config)
	accountController := controllers.NewAccountController(config)

	commonHandlers := alice.New(
		middleware.NewLogMiddleware().Run,
		middleware.NewMongoDbMiddleware(config).Run,
	)

	authenticatedHandlers := alice.New(
		middleware.NewLogMiddleware().Run,
		middleware.NewMongoDbMiddleware(config).Run,
		middleware.NewAuthenticationMiddleware().Run,
	)

	router := mux.NewRouter()
	router.Handle("/login", commonHandlers.ThenFunc(authController.Login))
	router.Handle("/validate-token", commonHandlers.ThenFunc(tokenController.Validate))
	router.Handle("/register", commonHandlers.ThenFunc(accountController.Register))
	router.Handle("/update-password", authenticatedHandlers.ThenFunc(accountController.UpdatePassword))
	return router
}
