package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"bitbucket.org/strom87/bouncer/models"
)

// AuthenticationController struct
type AuthenticationController struct {
	Controller
}

// NewAuthenticationController returns an instance of AuthenticationController
func NewAuthenticationController(config config.Config) *AuthenticationController {
	return &AuthenticationController{Controller{config}}
}

// Login route to login user
func (c AuthenticationController) Login(w http.ResponseWriter, r *http.Request) {
	var model models.Login
	if !c.IsValidInput(w, r, &model, "AuthenticationController", "Login") {
		return
	}

	user, response := core.NewAuthentication().ValidateCredentials(
		repositories.NewUserRepository(c.Connection(r)),
		model,
		core.NewHelper().RandString(c.config.Jwt.ControlValueLength),
	)

	if response != constants.Ok {
		log.Println("AuthenticationController", "Login", "ValidateCredentials", response)
		c.Encode(w, models.NewErrorResponse(response))
		return
	}

	token, err := core.NewJwtHandler(c.config).CreateToken(user, model.Platform)
	if err != nil {
		log.Println("AuthenticationController", "Login", "CreateToken", err)
		c.Encode(w, models.NewErrorResponse(constants.TokenCreateError))
		return
	}

	c.Encode(w, models.NewSuccessResponse(token))
}
