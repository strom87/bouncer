package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"bitbucket.org/strom87/bouncer/models"
)

// TokenController struct
type TokenController struct {
	Controller
}

// NewTokenController returns an instance of TokenController
func NewTokenController(config config.Config) *TokenController {
	return &TokenController{Controller{config}}
}

// Validate validates the user token
func (c TokenController) Validate(w http.ResponseWriter, r *http.Request) {
	var model models.Validate
	if !c.IsValidInput(w, r, &model, "TokenController", "Validate") {
		return
	}

	user, code, err := core.NewJwtHandler(c.config).ValidateToken(
		repositories.NewUserRepository(c.Connection(r)),
		model.Token,
	)
	if err != nil {
		log.Println("TokenController", "Validate", "ValidateToken", err)
		c.Encode(w, models.NewErrorResponse(code))
		return
	} else if user == nil {
		log.Println("TokenController", "Validate", "ValidateToken", "User not found")
		c.Encode(w, models.NewErrorResponse(constants.UserNotFound))
		return
	}

	c.Encode(w, user)
}
