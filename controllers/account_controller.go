package controllers

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"bitbucket.org/strom87/bouncer/factories"
	"bitbucket.org/strom87/bouncer/models"
)

// AccountController struct
type AccountController struct {
	Controller
}

// NewAccountController returns an instance of AccountController
func NewAccountController(config config.Config) *AccountController {
	return &AccountController{Controller{config}}
}

// Register route to register new users
func (c AccountController) Register(w http.ResponseWriter, r *http.Request) {
	var model models.Register
	if !c.IsValidInput(w, r, &model, "AuthenticationController", "Register") {
		return
	}

	u, _ := repositories.NewUserRepository(c.Connection(r)).GetByEmail(model.Email)
	if u != nil {
		log.Println("AuthenticationController", "Register", "GetByEmail", "User already exists")
		c.Encode(w, models.NewErrorResponse(constants.UserExists))
		return
	}

	user, err := factories.NewRegisterFactory().Create(model)
	if err != nil {
		log.Println("AuthenticationController", "Register", "Create", err)
		c.Encode(w, models.NewErrorResponse(constants.UnexpectedError))
		return
	}

	if err := repositories.NewUserRepository(c.Connection(r)).Insert(user); err != nil {
		log.Println("AuthenticationController", "Register", "Insert", err)
		c.Encode(w, models.NewErrorResponse(constants.UnexpectedError))
		return
	}

	c.Encode(w, models.NewSuccessResponse(""))
}

// UpdatePassword change user password
func (c AccountController) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	var model models.UpdatePassword
	if !c.IsValidInput(w, r, &model, "AuthenticationController", "UpdatePassword") {
		return
	}

	err := core.NewAccount().UpdatePassword(
		repositories.NewUserRepository(c.Connection(r)),
		model.ID,
		model.Password,
	)
	if err != nil {
		log.Println("AuthenticationController", "UpdatePassword", "UpdatePassword", err)
		c.Encode(w, models.NewErrorResponse(constants.UserNotFound))
		return
	}

	c.Encode(w, models.NewSuccessResponse(""))
}
