package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database"
	"bitbucket.org/strom87/bouncer/models"
	"github.com/gorilla/context"
	validator "github.com/strom87/validator-go"
)

// Controller struct
type Controller struct {
	config config.Config
}

// Connection returns a connection to mongodb from the Request context
func (Controller) Connection(r *http.Request) *database.MongoDbConnection {
	if conn := context.Get(r, "connection"); conn != nil {
		return conn.(*database.MongoDbConnection)
	}

	log.Println("No mongodb connection in http context")
	return nil
}

// Decode decodes json to struct model
func (Controller) Decode(r *http.Request, value interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(value); err != nil {
		return err
	}
	return nil
}

// Encode encodes a struct model to json
func (Controller) Encode(w http.ResponseWriter, value interface{}) error {
	if err := json.NewEncoder(w).Encode(value); err != nil {
		return err
	}
	return nil
}

// DecodeInput decodes user input
func (c Controller) DecodeInput(w http.ResponseWriter, r *http.Request, v interface{}, controller, function string) bool {
	if err := c.Decode(r, v); err != nil {
		log.Println(controller, function, "Decode", err)
		c.Encode(w, models.NewErrorResponse(constants.JSONDecodeError))
		return false
	}
	return true
}

// IsValidInput validates user input and parses user input
func (c Controller) IsValidInput(w http.ResponseWriter, r *http.Request, v interface{}, controller, function string) bool {
	if !c.DecodeInput(w, r, v, controller, function) {
		return false
	}

	if isValid, errorMessages, err := validator.Validate(v); !isValid {
		if err != nil {
			log.Println(controller, function, "Validate", err)
		} else {
			log.Println(controller, function, "Validate", "Invalid user input")
		}
		c.Encode(w, models.NewErrorDataResponse(errorMessages, constants.InvalidUserInput))
		return false
	}
	return true
}
