package main

import (
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/routes"
)

func main() {
	config := config.New()
	router := routes.Init(config)

	http.Handle("/", router)
	http.ListenAndServe(":"+config.Port, nil)
}
