package middleware

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/database"
	"github.com/gorilla/context"
)

// MongoDbMiddleware struct
type MongoDbMiddleware struct {
	config config.Config
}

// NewMongoDbMiddleware returns an instance of MongoDbMiddleware
func NewMongoDbMiddleware(config config.Config) *MongoDbMiddleware {
	return &MongoDbMiddleware{config}
}

// Run the middleware handler
func (m *MongoDbMiddleware) Run(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		connection := database.NewMongoDbConnection(m.config)
		err := connection.Open()
		if err != nil {
			log.Println(err)
			return
		}
		defer connection.Close()

		context.Set(r, "connection", connection)
		next.ServeHTTP(w, r)
	})
}
