package middleware

import (
	"log"
	"net/http"

	"bitbucket.org/strom87/bouncer/config"
	"bitbucket.org/strom87/bouncer/core"
	"bitbucket.org/strom87/bouncer/core/constants"
	"bitbucket.org/strom87/bouncer/database"
	"bitbucket.org/strom87/bouncer/database/repositories"
	"bitbucket.org/strom87/bouncer/models"
	"github.com/gorilla/context"
)

// AuthenticationMiddleware struct
type AuthenticationMiddleware struct{}

// NewAuthenticationMiddleware returns an instance of AuthenticationMiddleware
func NewAuthenticationMiddleware() *AuthenticationMiddleware {
	return &AuthenticationMiddleware{}
}

// Run the middleware handler
func (AuthenticationMiddleware) Run(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn := context.Get(r, "connection").(*database.MongoDbConnection)
		if conn == nil {
			log.Println("AuthenticationMiddleware", "Run", "Connection", "No db connection")
			models.NewErrorResponse(constants.DbConnectionError)
			return
		}

		_, code, err := core.NewJwtHandler(config.New()).ValidateToken(
			repositories.NewUserRepository(conn),
			r.Header.Get("bearer"),
		)
		if err != nil {
			log.Println("AuthenticationMiddleware", "Run", "ValidateToken", err)
			models.NewErrorResponse(code)
			return
		}

		next.ServeHTTP(w, r)
	})
}
