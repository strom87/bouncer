package factories

import (
	"bitbucket.org/strom87/bouncer/database/entities"
	"bitbucket.org/strom87/bouncer/models"
	"github.com/strom87/encryption"
)

// RegisterFactory struct
type RegisterFactory struct{}

// NewRegisterFactory returns an instance of RegisterFactory
func NewRegisterFactory() *RegisterFactory {
	return &RegisterFactory{}
}

// Create returns a user entity mapped from the register model
func (RegisterFactory) Create(model models.Register) (*entities.User, error) {
	password, salt, err := encryption.NewPasswordHash().Make(model.Password)
	if err != nil {
		return nil, err
	}

	return &entities.User{
		FirstName: model.FirstName,
		LastName:  model.LastName,
		Email:     model.Email,
		Password:  password,
		Salt:      salt,
		IsActive:  true,
	}, nil
}
