package config

import (
	"io/ioutil"
	"time"

	"gopkg.in/yaml.v2"
)

// Config maps the fields and values in server.yml file
type Config struct {
	Port    string  `yaml:"port"`
	Rsa     Rsa     `yaml:"rsa"`
	Jwt     Jwt     `yaml:"jwt"`
	MongoDb MongoDb `yaml:"mongo_db"`
}

// Rsa maps the rsa files
type Rsa struct {
	PublicKey  string `yaml:"public_key"`
	PrivateKey string `yaml:"private_key"`
}

// MongoDb configuration mapping
type MongoDb struct {
	Database       string        `yaml:"database"`
	Username       string        `yaml:"username"`
	Password       string        `yaml:"password"`
	Address        []string      `yaml:"address"`
	TimeoutSeconds time.Duration `yaml:"timeout_seconds"`
}

// Jwt mapps json web token configuration
type Jwt struct {
	ExpirationMinutes  time.Duration `yaml:"expiration_minutes"`
	ControlValueLength int           `yaml:"control_value_length"`
}

// New returns a mapped struct with all the values from the server.yml file
func New() Config {
	source, err := ioutil.ReadFile("config/server.yml")
	if err != nil {
		panic(err)
	}

	var config Config
	err = yaml.Unmarshal(source, &config)
	if err != nil {
		panic(err)
	}

	return config
}
