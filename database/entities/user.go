package entities

import "gopkg.in/mgo.v2/bson"

// User database structure
type User struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	FirstName string        `json:"first_name" bson:"first_name"`
	LastName  string        `json:"last_name" bson:"last_name"`
	Email     string        `json:"email" bson:"email"`
	Password  string        `json:"-" bson:"password"`
	Salt      string        `json:"-" bson:"salt"`
	IsActive  bool          `json:"-" bson:"is_active"`
	Web       Platform      `json:"-" bson:"web"`
	App       Platform      `json:"-" bson:"app"`
}
