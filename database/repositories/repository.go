package repositories

import (
	"bitbucket.org/strom87/bouncer/database"
	mgo "gopkg.in/mgo.v2"
)

// Repository struct
type Repository struct {
	connection *database.MongoDbConnection
	collection string
}

// Collection returns the database collection for mongodb
func (r Repository) Collection() *mgo.Collection {
	return r.connection.Session.DB(r.connection.Config.MongoDb.Database).C(r.collection)
}
