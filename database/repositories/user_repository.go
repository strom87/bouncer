package repositories

import (
	"bitbucket.org/strom87/bouncer/database"
	"bitbucket.org/strom87/bouncer/database/entities"
	"gopkg.in/mgo.v2/bson"
)

// UserRepository struct
type UserRepository struct {
	Repository
}

// NewUserRepository returns an instance of UserRepository
func NewUserRepository(connection *database.MongoDbConnection) *UserRepository {
	return &UserRepository{Repository{connection, "users"}}
}

// GetById find user by id
func (u UserRepository) GetById(id string) (*entities.User, error) {
	var user entities.User
	if err := u.Collection().Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

// GetByEmail find user by email
func (u UserRepository) GetByEmail(email string) (*entities.User, error) {
	var user entities.User
	if err := u.Collection().Find(bson.M{"email": email}).One(&user); err != nil {
		return nil, err
	}

	return &user, nil
}

// Insert adds a record to the user collection
func (u UserRepository) Insert(user *entities.User) error {
	return u.Collection().Insert(user)
}

// Update updates a record in the user collection
func (u UserRepository) Update(user *entities.User) error {
	return u.Collection().Update(bson.M{"_id": user.ID}, user)
}
