package models

// UpdatePassword struct
type UpdatePassword struct {
	ID              string `json:"id" validator:"required"`
	Password        string `json:"password" validator:"required|min:6|max:50|equals:ConfirmPassword"`
	ConfirmPassword string `json:"confirm_password"`
}

// NewUpdatePassword returns an instance of UpdatePassword
func NewUpdatePassword() *UpdatePassword {
	return &UpdatePassword{}
}
