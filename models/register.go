package models

// Register model
type Register struct {
	FirstName       string `json:"first_name" validator:"between:1,50"`
	LastName        string `json:"last_name" validator:"between:1,50"`
	Email           string `json:"email" validator:"email"`
	Password        string `json:"password" validator:"required|min:6|max:50|equals:ConfirmPassword"`
	ConfirmPassword string `json:"confirm_password"`
}
