package models

// Login model
type Login struct {
	Email    string `json:"email" validator:"email"`
	Password string `json:"password" validator:"required|min:6|max:50"`
	Platform string `json:"platform" validator:"required"`
}
