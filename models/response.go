package models

import (
	"log"

	"bitbucket.org/strom87/bouncer/core/constants"
)

// Response model
type Response struct {
	Data       interface{}            `json:"data,omitempty"`
	Code       constants.ResponseCode `json:"code"`
	CodeString string                 `json:"code_string"`
	Success    bool                   `json:"success"`
}

// NewSuccessResponse returns a success response model
func NewSuccessResponse(data interface{}) *Response {
	return &Response{
		Data:       data,
		Code:       constants.Ok,
		CodeString: constants.Ok.ToString(),
		Success:    true,
	}
}

// NewErrorResponse returns a error response model
func NewErrorResponse(code constants.ResponseCode) *Response {
	log.Println(code)
	return &Response{
		Data:       nil,
		Code:       code,
		CodeString: code.ToString(),
		Success:    false,
	}
}

// NewErrorDataResponse returns a error response model
func NewErrorDataResponse(data interface{}, code constants.ResponseCode) *Response {
	log.Println(code)
	return &Response{
		Data:       data,
		Code:       code,
		CodeString: code.ToString(),
		Success:    false,
	}
}
